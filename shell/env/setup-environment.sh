#!/bin/bash

clear
set +v

DEFAULT_ANSWER="yes"
ANSWER=""

setupEnvironment() {
  printf "\n${GREEN}Do you want to setup the git repository? ${NC}"
  read -e -p "(yes) " ANSWER
  echo ""

  ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
  if [ "${ANSWER}" == "yes" ] ; then
    echo -e "${BLUE}Setting up git repository.${NC}"

    git init

    cp -fr $DEVPRE_HOME/template/git-repo/.editorconfig ./
    cp -fr $DEVPRE_HOME/template/git-repo/.gitattributes ./
    cp -fr $DEVPRE_HOME/template/git-repo/CHANGELOG ./CHANGELOG
    cp -fr $DEVPRE_HOME/template/git-repo/LICENSE ./LICENSE
    cp -fr $DEVPRE_HOME/template/git-repo/README.adoc ./

    VERSION=`date +%Y.%U.0.`
    sed -i "" -e "s/202x.xx.0/${VERSION}/g" CHANGELOG

    mkdir -p ./.gitlab/issue_templates
    mkdir -p ./.gitlab/merge_request_templates
    cp -fr $DEVPRE_HOME/template/git-repo/issue/feature.md ./.gitlab/issue_templates/feature.md
    cp -fr $DEVPRE_HOME/template/git-repo/issue/bug.md ./.gitlab/issue_templates/bug.md
    cp -fr $DEVPRE_HOME/template/git-repo/merge/basic.md ./.gitlab/merge_request_templates/basic.md

    cp -fr $DEVPRE_HOME/template/git-repo/hook/prepare-commit-msg ./.git/hooks/prepare-commit-msg
    chmod 755 ./.git/hooks/prepare-commit-msg

    GIT_IGNORE_DEFAULT="linux,macos,windows,jetbrains+all,visualstudiocode,vim"

    printf "\n%s\n" "------------------------------------------------------------------------------"
    git ignore list
    printf "\n%s\n" "------------------------------------------------------------------------------"

    printf "\n${GREEN}Please input git ignore list(default: ${GIT_IGNORE_DEFAULT}): ${NC}"
    read -e -p "" GIT_IGNORE
    git ignore ${GIT_IGNORE_DEFAULT},${GIT_IGNORE} > .gitignore

    GIT_REMOTE_DEFAULT=""
    printf "\n${GREEN}Please input your remote git remote repository address: ${NC}"
    read -e -p "" GIT_REMOTE
    echo ""

    GIT_REMOTE="${GIT_REMOTE:-${GIT_REMOTE_DEFAULT}}"
    if [ "${GIT_REMOTE}" != "" ] ; then
      git remote add origin ${GIT_REMOTE}
    fi

    printf "\n${GREEN}Initialize repository."
    git commit --allow-empty -m "Initial repository"

    printf "Mission completed.\n\n"
  else
    printf "Skip setup the git repository.\n\n"
  fi
}
