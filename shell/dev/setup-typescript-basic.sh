#!/bin/bash


clear
set +v

. $DEVPRE_HOME/shell/env/setup-environment.sh

DEFAULT_ANSWER="yes"
ANSWER=""

DEFAULT_PROJECT_NAME=""
PROJECT_NAME=""
PROJECT_INIT_VERSION="$(date +%Y).$(date +%V).0"

setupTypeScriptBasic() {
  printf "\n${GREEN}Do you want to install the typescript project? ${NC}"
  read -e -p "(yes) " ANSWER
  echo ""

  ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
  DEFAULT_PROJECT_NAME=${PWD##*/}

  if [ "${ANSWER}" == "yes" ] ; then

    while [ "${PROJECT_NAME}" == "" ]
    do
      printf "${GREEN}Project name ${NC}"
      read -e -p "(default: ${DEFAULT_PROJECT_NAME}): " PROJECT_NAME
      PROJECT_NAME="${PROJECT_NAME:-${DEFAULT_PROJECT_NAME}}"
    done
    echo ""


    echo -e "${BLUE}Setting up typescript project.${NC}"

    npm init -y

    printf "${BLUE}Installing typescript local module.${NC}\n"
    npm i typescript tsconfig-paths ts-node 
    npm i -D @types/node
    echo ""

    printf "${BLUE}Installing environment module.${NC}\n"
    npm i cross-env nodemon pino pino-pretty 
    npm i -D eslint prettier eslint-config-prettier eslint-plugin-prettier
    npm i -D @typescript-eslint/parser @typescript-eslint/eslint-plugin @types/pino
    echo ""

    printf "${BLUE}Setting up typescript modules.${NC}\n"
    cp -fr $DEVPRE_HOME/template/typescript/tsconfig.json ./
    echo ""

    printf "${BLUE}Setting up default project structure.${NC}\n"
    if [ ! -d ./src/main/ ] ; then
      mkdir -p ./src/main
      cp -fr $DEVPRE_HOME/template/typescript/src/main/app.init.ts ./src/main/app.init.ts
      cp -fr $DEVPRE_HOME/template/typescript/src/main/app.ts ./src/main/app.ts
      mkdir -p ./src/resource
    fi

    if [ ! -d ./src/main/util/ ] ; then
      mkdir -p ./src/main/util
      cp -fr $DEVPRE_HOME/template/typescript/src/main/util/logger.ts ./src/main/util/logger.ts
    fi

    if [ ! -d ./configure/nodemon/ ] ; then
      mkdir -p ./configure/nodemon
      cp -fr $DEVPRE_HOME/template/typescript/configure/nodemon/debug.json ./configure/nodemon/debug.json
      cp -fr $DEVPRE_HOME/template/typescript/configure/nodemon/dev.json ./configure/nodemon/dev.json
      cp -fr $DEVPRE_HOME/template/typescript/configure/nodemon/production.json ./configure/nodemon/production.json
    fi

    cp -fr $DEVPRE_HOME/template/typescript/.eslintignore ./.eslintignore
    cp -fr $DEVPRE_HOME/template/typescript/.eslintrc ./.eslintrc
    cp -fr $DEVPRE_HOME/template/typescript/.prettierrc ./.prettierrc

    echo ""

    printf "${BLUE}Updating package.json${NC}\n"
    npm i json
    ./node_modules/.bin/json -I -f package.json -e 'this.name="'${PROJECT_NAME}'"'
    ./node_modules/.bin/json -I -f package.json -e 'this.version="'${PROJECT_INIT_VERSION}'"'
    ./node_modules/.bin/json -I -f package.json -e 'this.main="src/main/app.init.ts"'
    ./node_modules/.bin/json -I -f package.json -e 'this.scripts.debug="nodemon --config configure/nodemon/debug.json"'
    ./node_modules/.bin/json -I -f package.json -e 'this.scripts.dev="nodemon --config configure/nodemon/dev.json"'
    ./node_modules/.bin/json -I -f package.json -e 'this.scripts.format="prettier --config .prettierrc 'src/**/*.ts' --write"'
    ./node_modules/.bin/json -I -f package.json -e 'this.scripts.lint="eslint . --ext .ts"'
    ./node_modules/.bin/json -I -f package.json -e 'this.scripts.prod="nodemon --config configure/nodemon/production.json"'
    ./node_modules/.bin/json -I -f package.json -e 'this.scripts.start="nodemon --config configure/nodemon/production.json"'
    ./node_modules/.bin/json -I -f package.json -e 'this.scripts.test="cross-env NODE_ENV=test & npm run lint & jest --coverage"'
    npm uninstall json
    echo ""

    printf "${BLUE}Installing testing library module.${NC}\n"
    npm i -D jest ts-jest tsconfig-paths-jest @types/jest
    echo ""

    cp -fr $DEVPRE_HOME/template/typescript/jest.config.js ./

    if [ ! -d ./src/test/ ] ; then
      mkdir -p ./src/test
      cp -fr $DEVPRE_HOME/template/typescript/src/test/app.spec.ts ./src/test/app.spec.ts
    fi

    setupEnvironment

    echo "Installed typescript project."
  else
    echo "Not installed typescript project."
  fi
}
