#!/bin/bash

clear
set +v

. $DEVPRE_HOME/shell/env/setup-environment.sh

DEFAULT_ANSWER="yes"
ANSWER=""

DEFAULT_PROJECT_NAME=""
PROJECT_NAME=""
PROJECT_INIT_VERSION="$(date +%Y).$(date +%V).0"

setupTypeScriptNuxt() {
  printf "\n${GREEN}Do you want to install the nuxt project? ${NC}"
  read -e -p "(yes) " ANSWER
  echo ""

  ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
  DEFAULT_PROJECT_NAME=${PWD##*/}

  if [ "${ANSWER}" == "yes" ] ; then

    while [ "${PROJECT_NAME}" == "" ]
    do
      printf "${GREEN}Project name ${NC}"
      read -e -p "(default: ${DEFAULT_PROJECT_NAME}): " PROJECT_NAME
      PROJECT_NAME="${PROJECT_NAME:-${DEFAULT_PROJECT_NAME}}"
    done
    echo ""


    echo -e "${BLUE}Setting up nuxt project.${NC}"
    NUXT_APP_RECIPE=$(cat <<EOF
    { \
      "name" : "${PROJECT_NAME}", \
      "author" : "wisoft.io <contact@wisoft.io>", \
      "language" : "ts", \
      "pm" : "npm", \
      "ui" : "element", \
      "server" : "none", \
      "features" : ["axios", "dotenv", "pwa"], \
      "linter" : ["eslint", "prettier"], \
      "test" : "jest", \
      "mode" : "universal", \
      "target" : "server", \
      "devTools" : "jsconfig.json", \
      "ci" : "none" \
    }
EOF
)
    npx create-nuxt-app ./ --overwrite-dir --answers "$NUXT_APP_RECIPE"

    setupEnvironment


    echo "Installed typescript project."
  else
    echo "Not installed typescript project."
  fi
}