#!/bin/bash


clear
set +v

. $DEVPRE_HOME/shell/env/setup-environment.sh

DEFAULT_ANSWER="yes"
ANSWER=""

DEFAULT_PROJECT_NAME=""
PROJECT_NAME=""

setupKotlin() {
  printf "\n${GREEN}Do you want to install the kotlin project? ${NC}"
  read -e -p "(yes) " ANSWER
  echo ""

  ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
  DEFAULT_PROJECT_NAME=${PWD##*/}
  if [ "${ANSWER}" == "yes" ] ; then

    while [ "${PROJECT_NAME}" == "" ]
    do
      printf "${GREEN}Project name ${NC}"
      read -e -p "(default: ${DEFAULT_PROJECT_NAME}): " PROJECT_NAME
      PROJECT_NAME="${PROJECT_NAME:-${DEFAULT_PROJECT_NAME}}"
    done
    echo ""
    echo -e "${BLUE}Setting up kotlin project.${NC}"

    gradle init --dsl kotlin --type basic --project-name ${PROJECT_NAME}

    cp -fr $DEVPRE_HOME/template/gradle/kotlin/gradle.properties ./gradle.properties
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/settings.gradle.kts ./settings.gradle.kts
    sed -i "" -e "s/wisoft-imsi/${PROJECT_NAME}/g" settings.gradle.kts


    mkdir -p buildSrc/src/main/kotlin/
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/buildSrc/build.gradle.kts ./buildSrc/build.gradle.kts
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/buildSrc/src/main/kotlin/versions.kt ./buildSrc/src/main/kotlin/versions.kt

    mkdir -p buildSrc/src/main/kotlin/conventions
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/buildSrc/src/main/kotlin/versions.kt ./buildSrc/src/main/kotlin/versions.kt
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/buildSrc/src/main/kotlin/conventions/*.gradle.kts ./buildSrc/src/main/kotlin/conventions

    mkdir -p buildSrc/src/main/kotlin/dependencies/main
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/buildSrc/src/main/kotlin/dependencies/main/*.gradle.kts ./buildSrc/src/main/kotlin/dependencies/main

    mkdir -p buildSrc/src/main/kotlin/dependencies/test
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/buildSrc/src/main/kotlin/dependencies/test/*.gradle.kts ./buildSrc/src/main/kotlin/dependencies/test

    mkdir -p ./app/src/main/kotlin
    mkdir -p ./app/src/main/resources
    mkdir -p ./app/src/test/kotlin
    mkdir -p ./app/src/test/resources
    cp -fr $DEVPRE_HOME/template/gradle/kotlin/app/build.gradle.kts ./app/build.gradle.kts

    setupEnvironment

    echo "Installed kotlin project."
  else
    echo "Not installed kotlin project."
  fi
}
