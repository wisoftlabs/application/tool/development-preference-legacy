const tsconfig = require("./tsconfig.json");
const moduleNameMapper = require("tsconfig-paths-jest")(tsconfig);

module.exports = {
  roots: ["<rootDir>/src"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  testEnvironment: "node",
  testRegex: "(/__test__/.*|(\\.|/)(test|spec))\\.tsx?$",
  coverageDirectory: "<rootDir>/dist/coverage/",
  coverageReporters: ["text-summary", "html", "json"],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  moduleNameMapper
};
