class App {
  private readonly name: string;

  constructor(name: string = "World!") {
    this.name = name;
  }

  public static bootstrap(name?: string): App {
    return new App(name);
  }

  public print(): string {
    return `Hello, ${this.name}`;
  }
}

export default App;
