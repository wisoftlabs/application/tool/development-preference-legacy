import pino from "pino";

function getLogLevel() {
  let logLevel;
  switch (process.env.NODE_ENV) {
    case "development":
      logLevel = "debug";
      break;
    case "production":
      logLevel = "info";
      break;
    default:
      logLevel = "trace";
  }

  return logLevel;
}

export const log = pino({
  name: "template",
  level: getLogLevel(),
  safe: true,
  prettyPrint: { colorize: true, translateTime: "SYS:standard" },
});
