import App from "~/src/main/app";
import { log } from "~/src/main/util/logger";

const app = App.bootstrap();

log.debug(`Current Environment Mode: ${process.env.NODE_ENV}`);
log.info(app.print());
