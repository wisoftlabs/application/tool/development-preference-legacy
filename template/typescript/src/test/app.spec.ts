import App from "~/src/main/app";

let app: App;

describe("T#001. Hello, World!", () => {
  test("Hello, World!", () => {
    app = App.bootstrap();
    expect(app.print()).toBe("Hello, World!");
  });

  test("Hello, InputName!", () => {
    app = App.bootstrap("InputName!");
    expect(app.print()).toBe("Hello, InputName!");
  });
});
