#!/bin/bash

clear
set +v
. $DEVPRE_HOME/shell/env/setup-environment.sh
# . $DEVPRE_HOME/shell/java/setup-java.sh
# . $DEVPRE_HOME/shell/java/setup-java-spring.sh
. $DEVPRE_HOME/shell/dev/setup-kotlin.sh
. $DEVPRE_HOME/shell/dev/setup-kotlin-spring.sh
. $DEVPRE_HOME/shell/dev/setup-typescript.sh
# . $DEVPRE_HOME/shell/typescript/setup-typescript-nest.sh
. $DEVPRE_HOME/shell/dev/setup-typescript-nuxt.sh

NC="\033[0m"
RED="\033[0;31m"
GREEN="\033[1;32m"
BLUE="\033[1;34m"
CYAN="\033[1;36m"

DEFAULT_ANSWER="yes"
ANSWER=""
COMMAND=""


while :
do
clear
  echo ""
  echo ""
  echo -e "${BLUE}The project initializer ${NC}"
  echo ""
  echo ""
  echo -e "--${CYAN}Java${NC}------------------------------------------------------------"
  echo -e "(11) Setting up Java project [${RED}Soon${NC}]"
  echo -e "(12) Setting up Java project with spring boot [${RED}Soon${NC}]"
  echo ""
  echo -e "--${CYAN}Kotlin${NC}----------------------------------------------------------"
  echo -e "(21) Setting up Kotlin project"
  echo -e "(22) Setting up Kotlin project with spring boot"
  echo ""
  echo -e "--${CYAN}TypeScript${NC}------------------------------------------------------"
  echo -e "(31) Setting up TypeScript project [${RED}Soon${NC}]"
  echo -e "(32) Setting up TypeScript project with NestJS [${RED}Soon${NC}]"
  echo -e "(33) Setting up TypeScript project with NuxtJS [${RED}Soon${NC}]"
  echo ""
  echo -e "--${CYAN}Environment${NC}-----------------------------------------------------"
  echo -e "(91) Setting up Project & VCS environment [${RED}Standalone${NC}]"
  echo ""
  echo "------------------------------------------------------------------"
  echo -e "${RED} (q) Exit${NC}"
  echo "------------------------------------------------------------------"
  echo ""
  printf "${GREEN}Please enter your choice: ${NC}"

  read COMMAND
  case "$COMMAND" in
    11) setupJava ;;
    12) setupJavaSpring ;;
    21) setupKotlin ;;
    22) setupKotlinSpring ;;
    31) setupTypeScript ;;
    32) setupTypeScriptNest ;;
    33) setupTypeScriptNuxt ;;
    91) setupEnvironment ;;
     q) echo -e "\n${BLUE}Thank you. Have a nice day.${NC}"; exit ;;
     *) echo -e "\n${RED}invalid option.${NC}"     ;;
  esac
done
